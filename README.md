# generic-ci

This repository contains a simple example application with two containers. The CI Pipeline uses a container that holds **docker**, **kubectl**, **kustomize**, **envsubst** and a few more capabilities. You can find this container on dockerhub <https://hub.docker.com/r/teutonet/ci-tools> and the inspect the corresponding Dockerfile here <https://github.com/TeutoNet/ci-tools>.

## Applications and Dockerfile(s)

In the project root you find two folder, **echo** and **echo2**. Each of those contains a file called **Dockerfile** responsible for building a container Image. Those two containers do the same thing: If accessed by a browser they respond by echoing back the HTTP(S)-Headers that were sent to them.

## k8s and kustomize

Furthermore there is a folder called **k8s** in this projects root. It contains YAML-Manifests that are supposed to be processed by a software called **kustomize** (more information available here: <https://github.com/kubernetes-sigs/kustomize> ). The job of kustomize is to gather all resources defined in a file called **kustomization.yaml** and generate one large YAML-Document that contains all the resources gathered and processed. In addition kustomize is used to dynamically set the container image names for the delpoyments as well as the correct namespace for all resources depending on the environment the CI-Job is run in.

### k8s structure base

The main kubernetes resource YAML-Documents are held inside **k8s/base**. There you'll find a deployment resource document for each container (echo and echo2). Furthermore there is a Service manifest for each application and one Ingress for the both of them.

### Deployments

The image names for these deployments will be set during CI-run inside the shell-function called **deployment()**. The kustomize command responsible for updating the images names is ```kustomize edit set image [image-to-override]=[new-image-name]```. The CI dynamically sets the new image name containing information about the registry where the image is held.

The deployments contain the following annotations:

```yaml
app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
```

If the kubernetes Clusters were to be GitLab managed Clusters and Deploy Boards and Webterminals were available in your GitLab Edition they would be allowing GitLab to access these resources for display of Deploy Boards and access to web terminals. The ${CI_PROJECT_PATH_SLUG} and ${CI_ENVIRONMENT_SLUG} are dynamically set by the CI-Pipeline.

### Services

The deployments each have their own services. Those services are then referenced by the ingress resource, depending on which URL path is chosen by the Users Browser.

### Ingress

The Ingress is configured to obtain a **Let's Encrypt** certificate through kubernetes' cert-manager. The **host** is set inside the **deployment()** shellfunction made available for the pipeline jobs. Depending on the environment to deploy in, a different host will be set for the **host**. It is derived from the environment url which is made available through $CI_ENVIRONMENT_URL and shortened using the Bash substring function (removing the <https://>).

## k8s structure overlays

Inside the **k8s/overlays** folder there is another directory for each environment the application will be deployed to, e.g. **dev**, **staging**, **prod**. Everyone of those directories contains its own **kustomization.yaml** file. When **kustomize** is asked to build the resources to be applied by **kubectl** inside the **.gitlab-ci.yml** file, the correct overlay folder with its **kustomization.yaml** is called, depending on the environment to be deployed to. A **kustomization.yaml** inside an overlay folder references the base-kustomization. Furter environment specific modifications could be added here, e.g. environment-specific cpu resources for a given deployment and so on.

## CI Variables

In order to function the CI pipeline needs access to a kubernetes Cluster. In a GitLab Managed Clusterenvironment you'd have a corresponding $KUBECONFIG made available automatically. In cases where the Kubernetes Cluster is not GitLab managed you'll be able to make your *kubeconfig* available through CI Variables. Keep in mind that the kubeconfig you provide needs to be equipped with the correct access rights to the used namespaces.

### Environment specific CI Variables

GitLab offers the possibility to set the CI Variables depending on the environment the CI job is run in. That means that if you were to use a different Kubernetes Cluster for your production environment than you'll have for dev and staging, you'd be able to set the $KUBECONFIG Variable with a different value for your production environment, no further logic inside the CI necessary.

### Image pull secret

The shell function **ensure_pull_secret()** enables CI jobs to verify a kubernetes secret containing access information for the image registry is available. If the secret is not available in the corresponding environment/namespace then it is created.

This function needs a user and a password that must be provided by your CI-Variables, namely **IMAGE_PULL_SECRET_PW** and **IMAGE_PULL_SECRET_USER**. You can create these values by creating a Deploy Token in your project under Settings => CI / CD => Deploy Tokens. Please choose **read_registry** for Scopes.

## .gitlab-ci.yml

You'll find that the **.gitlab-ci.yml** is thoroughly documented. The different sections and functions are examined in great detail and will guide you through the process.

## Development Flow

One would usually start by creating a **feature** branch (with the corresponding issue-id in its name) and work changes on it. When done the developer merges the changes into **dev**. Since there is an environment for **dev** the results can be observed and testet. After that the dev Branch is merged into Master which triggers a deployment to the **staging** environment. When this is all set and done a tag on the Master Branch needs to created to trigger a deployment to production.
